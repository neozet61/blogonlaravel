<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

//public

Route::get('/', [\App\Http\Controllers\PostController::class,"index"]);

Route::get('/post', [\App\Http\Controllers\PostController::class,"show"]);

Route::post('/save_comment', [\App\Http\Controllers\PostController::class,"storeComment"]);

//admin-category

Route::get('/admin', [\App\Http\Controllers\AdminCategoriesController::class,"index"]);

Route::get('/admin/create_category', function (){
    return view('category_form');
});

Route::get('/admin/edit_category', [\App\Http\Controllers\AdminCategoriesController::class,"edit"]);

Route::post('/admin/save_category', [\App\Http\Controllers\AdminCategoriesController::class,"save"]);

Route::get('/admin/delete_category', [\App\Http\Controllers\AdminCategoriesController::class,"delete"]);

Route::post('/admin/store_category', [\App\Http\Controllers\AdminCategoriesController::class,"store"]);

//admin-post

Route::get('/admin/posts', [\App\Http\Controllers\AdminPostsController::class,"index"]);

Route::get('/admin/create_post', [\App\Http\Controllers\AdminPostsController::class,"create"]);

Route::get('/admin/edit_post', [\App\Http\Controllers\AdminPostsController::class,"edit"]);

Route::post('/admin/save_post', [\App\Http\Controllers\AdminPostsController::class,"save"]);

Route::get('/admin/delete_post', [\App\Http\Controllers\AdminPostsController::class,"delete"]);


//admin-comment

Route::get('/admin/comments', [\App\Http\Controllers\AdminCommentsController::class,"index"]);

/*Route::get('/admin/create_comment', [\App\Http\Controllers\AdminCommentsController::class,"create"]);*/

Route::get('/admin/edit_comment', [\App\Http\Controllers\AdminCommentsController::class,"edit"]);

Route::post('/admin/update_comment', [\App\Http\Controllers\AdminCommentsController::class,"update"]);

Route::get('/admin/delete_comment', [\App\Http\Controllers\AdminCommentsController::class,"delete"]);

/*Route::post('/admin/store_comment', [\App\Http\Controllers\AdminCommentsController::class,"store"]);*/

