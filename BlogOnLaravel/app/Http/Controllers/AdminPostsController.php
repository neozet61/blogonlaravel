<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminPostsController extends Controller
{
    public function index(Request $request)
    {
        $allPosts = DB::table('posts')
            ->select(['posts.*', 'categories.title as category_title'])
            ->leftJoin('categories', 'categories.id', '=', 'posts.category_id')
            ->get();

        return view('admin_post', [
            'allPosts' => $allPosts
        ]);
    }

    public function create(Request $request)
    {

        return view('post_form', []);
    }

    public function delete(Request $request)
    {
        $postId = $request->get('post_id');

        $post = DB::table('posts')
            ->where('id', '=', $postId)->delete();

        return redirect('/admin/posts');
    }

/*    public function store(Request $request)
    {

        $newTitle = $request->get('new-post-title');
        $newCategory = $request->get('new-post-category');
        $newBody = $request->get('comment-input-text');
        $date = time();

        $array = [
            "title" => $newTitle,
            "body" => $newBody,
            "time_code" => $date,
            "category_id" => $newCategory
        ];

        DB::table('posts')->insert($array);

        return redirect('/admin/posts');
    }*/

public function save(Request $request)
    {
        $data = [
            "title" => $request->get('new-post-title'),
            "body" => $request->get('comment-input-text'),
            "category_id" => $request->get('new-post-category')
        ];

        $queryBuilder = DB::table('posts');

        if ($id = $request->get('id')) {
            $queryBuilder->where('id', '=', $id);

            $queryBuilder->update($data);
        } else {
            $data['time_code'] = time();

            $queryBuilder->insert($data);
        }

        return redirect('/admin/posts');
    }

    public function edit(Request $request)
    {
        $postId = $request->get('post_id');

        $post = DB::table('posts')
            ->where('id', '=', $postId)
            ->get()
            ->first();

        return view('post_form', [
            'post' => $post
        ]);
    }
}
