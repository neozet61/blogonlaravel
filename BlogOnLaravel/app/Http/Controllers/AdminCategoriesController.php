<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminCategoriesController extends Controller
{
    public function index(Request $request)
    {
        /*$allCategories = DB::table('categories')->get();*/

        return view('admin_category', [
            /*'allCategories ' => $allCategories*/
        ]);
    }

    public function delete(Request $request)
    {
        $categoryId = $request->get('category_id');

        DB::table('categories')->where('id', '=', $categoryId)->delete();

        return redirect('/admin');
    }

    /*    public function store(Request $request)
        {

            $newTitle = $request->get('new-category-title');

            $array = [
                "title" => $newTitle
            ];

            DB::table('categories')->insert($array);

            return redirect('/admin');
        }*/

    public function save(Request $request)
    {
        $title = $request->get('category-title');

        $queryBuilder = DB::table('categories');

        if ($id = $request->get('id')) {

            $queryBuilder->where('id', '=', $id)->update(['title' => $title]);

        } else {

            $queryBuilder->insert(['title' => $title]);
        }

        return redirect('/admin');
    }

    public function edit(Request $request)
    {
        $categoryId = $request->get('category_id');

        $category = DB::table('categories')
            ->where('id', '=', $categoryId)
            ->get()
            ->first();

        return view('category_form', [
            'category' => $category
        ]);
    }
}
