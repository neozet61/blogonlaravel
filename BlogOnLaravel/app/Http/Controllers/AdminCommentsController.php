<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminCommentsController extends Controller
{
    public function index()
    {
        $allComments = DB::table('comments')

            ->get();

        return view('admin_comment', [
            'allComments' => $allComments
        ]);
    }

    public function delete(Request $request)
    {
        $commentId = $request->get('comment_id');

        DB::table('comments')
            ->where('id', '=', $commentId)->delete();

        return redirect('/admin/comments');
    }

    public function update(Request $request)
    {
        $commentId = $request->get('id');
        $newGuestName = $request->get('update-comment-guest-name');
        $newText = $request->get('update-comment-text');

        $array = [
            'guest_name' => $newGuestName,
            'comment_text' => $newText
        ];

        DB::table('comments')
            ->where('id', '=', $commentId)
            ->update($array);

        return redirect('/admin/comments');
    }

    public function edit(Request $request)
    {
        $commentId = $request->get('comment_id');

        $comment = DB::table('comments')
            ->where('id', '=', $commentId)
            ->get()
            ->first();

        return view('edit_comment', [
            'comment' => $comment
        ]);
    }
}
