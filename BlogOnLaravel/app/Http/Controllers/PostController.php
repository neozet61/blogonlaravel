<?php

namespace App\Http\Controllers;

use App\Records;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request $request)
    {
        $currentPage = $request->get('page', 1);

        $queryBuilder = DB::table('posts')
            ->select(['posts.*', 'categories.title as category_title'])
            ->leftJoin('categories', 'categories.id', '=', 'posts.category_id');


        if ($categoryId = $request->get('category_id')) {
            $category = $queryBuilder
                ->where('category_id', '=', $categoryId)
                ->get()
                ->first();

            if (is_null(!$category)) {
                abort(404, "Данной категории не существует");
            }

        }

        $totalPage = ceil($queryBuilder->count() / 10);

        $allPosts = $queryBuilder
            ->orderBy('time_code', 'DESC')
            ->limit(10)
            ->offset(10 * ($currentPage - 1))
            ->get();

        return view('index', [
            'allPosts' => $allPosts,
            'totalPage' => $totalPage,
            'currentPage' => $currentPage,
            'category' => $category ?? null
        ]);
    }

    public function show(Request $request)
    {

        $currentPostId = $request->get('post_id');
        $lastVizit = $request->cookie('lastvizit_'.$currentPostId);

        $post = DB::table('posts')
            ->leftJoin('categories', 'categories.id', '=', 'posts.category_id')
            ->select(['posts.*', 'categories.title as category_title'])
            ->where('posts.id', '=', $currentPostId)
            ->get()
            ->first();

        if (!$post) {
            abort(404, "пост не найден");
        }

        $comments = DB::table('comments')
            ->where('post_id', '=', $currentPostId)
            ->orderBy('time_at', 'asc')
            ->get();

        $view = view('show_post', [
            'post' => $post,
            'comments' => $comments,
            'lastvizit' => $lastVizit
        ]);

        $response = new Response($view);

        $response->withCookie(cookie('lastvizit_' . $currentPostId, time()));

        return $response;
    }

    public function storeComment(Request $request)
    {

        $guestName = $request->get('comment-input-name');
        $commentText = $request->get('comment-input-text');
        $postId = $request->get('post_id');

        $array = [
            "guest_name" => $guestName,
            "comment_text" => $commentText,
            "time_at" => time(),
            "post_id" => $postId
        ];

        DB::table('comments')->insert($array);

        return redirect('/post?post_id=' . $postId);
    }
}
