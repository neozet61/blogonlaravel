<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', function ($view){
            $allCategories = DB::table('categories')->get();
            $lastComments = DB::table('comments')
                ->leftJoin('posts', 'comments.post_id', '=', 'posts.id')
                ->select(['comments.*', 'posts.title as post_title'])
                ->limit(5)
                ->orderBy('time_at', 'DESC')
                ->get();

            $view->with('allCategories', $allCategories);
            $view->with('lastComments', $lastComments);
        });
    }
}
