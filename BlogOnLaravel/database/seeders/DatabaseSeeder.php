<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($k = 1; $k <= 5; $k++) {
            \DB::table('categories')
                ->insert([
                    'id' => $k,
                    'title' => $faker->sentence(2)
                ]);

            for ($i = 1; $i <= 10; $i++) {
                \DB::table('posts')
                    ->insert([
                        'title' => $faker->sentence(4),
                        'body' => $faker->text(),
                        'time_code' => $faker->unixTime(),
                        'category_id' => $k
                    ]);
            }
        }
    }
}
