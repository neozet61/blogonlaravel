@extends('layouts/admin_base')
@section('content')
    <!--    content-->
    <div class="row">
        <div class="col-md-12">

            {{--<div class="create d-flex flex-row-reverse">
                <div class="p-2">
                    <a href="/admin/create_post" class="btn btn-success">+ Create Post</a>
                </div>
            </div>--}}

            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Guest Name</th>
                    <th>Commment</th>
                    <th>Actions</th>
                </tr>

                @foreach($allComments as $comment)
                    <tr>
                        <td>
                            {{$comment->id}}
                        </td>
                        <td>
                            {{$comment->guest_name}}
                        </td>
                        <td>
                            {{$comment->comment_text}}
                        </td>
                        <td>
                            <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                                <a type="button" href="/admin/edit_comment?comment_id={{$comment->id}}"
                                   class="btn btn-warning">Edit</a>
                                <a type="button" href="/admin/delete_comment?comment_id={{$comment->id}}"
                                   class="btn btn-danger">Delete</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
    @endsection
    </div>
    </body>
    </html>
