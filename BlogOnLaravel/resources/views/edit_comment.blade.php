@extends('layouts.admin_form_base')
@section('content')
    <!--    content-->
    <div class="row">
        <div class="col-md-12">
            <h2 id="edit-comment">Edit comment: {{$comment->guest_name}}</h2>

            <form id="edit-comment-form" method="post" action="/admin/update_comment">
                @csrf
                <div class="mb-3">
                    <label for="update-comment-guest-name" class="form-label">Guest Name</label>
                    <input value="{{$comment->guest_name}}" type="text" name="update-comment-guest-name" class="form-control"
                           id="update-comment-guest-name">
                    <input type="hidden" name="id" value="{{$comment->id}}">
                </div>
                <div class="mb-3">
                    <label for="update-comment-text" class="form-label">Body</label>
                    <textarea placeholder="Write you post here..." name="update-comment-text" class="form-control"
                              id="update-comment-text">{{$comment->comment_text}}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Edit comment</button>
            </form>
        </div>
    </div>
    @endsection
    </div>
    </body>
    </html>
