@extends('layouts/base')
@section('content')

    @if(isset($category))
        <div class="alert alert-info" role="alert">
            Now is showing the posts only from category <strong>{{$category->category_title}}</strong>. You can go
            back to <a href="/" class="alert-link">homepage</a>.
        </div>
    @endif

    @foreach($allPosts as $post)
        <!-- post-->
        <div class="post-preview">
            <h2 class="post-preview-title">
                {{$post->title}}
            </h2>
            <div class="post-preview-meta text-muted">
                <ul class="list-inline">
                    <li class="datetime list-inline-item">Date: {{date('d.m.Y H:m:s',$post->time_code)}}</li>
                    <li class="list-inline-item">Category:
                        <a href="/?category_id={{$post->category_id}}">{{$post->category_title}}</a></li>
                </ul>
            </div>
            <div class="post-preview-body">
                {{$post->body}}
            </div>
            <div class="post-preview-buttons">
                <a href="/post?post_id={{$post->id}} " class="btn btn-primary">Open</a>
            </div>
        </div>
    @endforeach


    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">


            @if($currentPage == 1)
                <li class="page-item disabled">
                    <a class="page-link">Previous</a>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="/?page={{$currentPage-1}}">Previous</a>
                </li>
            @endif
            @for($i = 1; $i <= $totalPage; $i++)
                @if($currentPage == $i)
                    <li class="page-item"><a class="page-link active">{{$i}}</a></li>
                @else
                    <li class="page-item"><a class="page-link" href="/?page={{$i}}">{{$i}}</a></li>
                @endif
            @endfor
                @if($currentPage == $totalPage)
                    <li class="page-item disabled">
                        <a class="page-link">Next</a>
                    </li>
                @else
                    <li class="page-item">
                        <a class="page-link" href="/?page={{$currentPage+1}}">Next</a>
                    </li>
            @endif
        </ul>
    </nav>
@endsection


