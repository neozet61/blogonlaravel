<!DOCTYPE html>
<html lang="en">
@section('head')
    <head>
        <meta charset="UTF-8">
        <title>My Blog</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD"
              crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
                crossorigin="anonymous"></script>
        <link href="/style.css" rel="stylesheet">
        <link href="admin.css" rel="stylesheet">
    </head>
@show
@section('h1')
    <body>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <h1 id="main-title">Admin Panel</h1>
                    </div>
                </div>
            </div>
        </div>
        @show
        @section('nav')
            <!-- navigation-->
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg bg-body-tertiary">
                        <div class="container-fluid">
                            <div class="collapse navbar-collapse" id="navbarNav">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link @if(isset($category)) active @endif"
                                           href="/admin">Categories</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link @if(isset($post)) active @endif"
                                           href="/admin/posts">Posts</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link @if(isset($comment)) active @endif" href="/admin/comments">Comments
                                            moderation</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        @show
        @section('content')

        @show
    </div>
    </body>
</html>
