<!DOCTYPE html>
<html lang="en">
@section('head')
    <head>
        <meta charset="UTF-8">
        <title>@section('title','My Blog')My Blog</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD"
              crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN"
                crossorigin="anonymous"></script>
        <link href="/style.css" rel="stylesheet">
    </head>
@show

<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-10">
                    <h1 id="main-title">My Blog Title</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-8">
            @section('content')



            @show

        </div>

        @section('categories')
            <div class="col-md-4">
                <div>
                    <div class="post-preview-buttons">
                        <a href="/admin" class="btn btn-primary">Admin</a>
                        <a href="/" class="btn btn-primary">Home</a>
                    </div>
                </div>
                <div id="sidebar-categories">
                    <h3 id="sidebar-categories-title">Categories</h3>
                    <ul>
                        @foreach($allCategories as $category)
                            @if(isset($categoryID))
                                @if($categoryID == $category->id)
                                    <li class="sidebar-categories-item active">{{$category->title}}</li>
                                @else
                                    <li class="sidebar-categories-item">
                                        <a href="/?category_id={{$category->id}}" >{{$category->title}}</a>
                                    </li>
                                @endif
                            @else
                                <li class="sidebar-categories-item">
                                    <a href="/?category_id={{$category->id}}" >{{$category->title}}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                @show
                @section('last_comments')
                    <div id="sidebar-comments">
                        <h3 id="sidebar-comments-title">Last Comments</h3>
                        @foreach($lastComments as $comment)
                            <div class="sidebar-comments-item">
                                <div class="sidebar-comments-item-title">
                                    <span class="nickname">{{$comment->guest_name}}</span>
                                    <a href="/post?post_id={{$comment->post_id}}#comment_{{$comment->id}}">commented</a>
                                    post
                                    <a href="/post?post_id={{$comment->post_id}}"
                                       class="post-title">{{$comment->post_title}}</a>:
                                </div>
                                <div class="sidebar-comments-item-body">
                                    {{$comment->comment_text}}
                                </div>
                            </div>
                        @endforeach

                        @show
                    </div>
            </div>

    </div>
</div>
</body>
</html>
