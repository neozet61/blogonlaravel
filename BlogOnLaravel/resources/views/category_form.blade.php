@extends('layouts.admin_form_base')
@section('content')

    <!--    content-->
    <div class="row">
        <div class="col-md-12">
            <h2 id="category-title">
                {{ !isset($category) ? "Create" : "Edit" }} category: {{ $category->title ?? '' }}
            </h2>

            <form id="category-form" method="post" action="/admin/save_category">
                @csrf
                <div class="mb-3">
                    <label for="category-title" class="form-label">Title</label>
                    <input placeholder="Title" type="text" name="category-title" class="form-control"
                           id="category-title" value="{{ $category->title ?? '' }}">
                    <input type="hidden" name="id" value="{{ $category->id ?? '' }}">
                </div>

                <button type="submit" class="btn btn-primary">Save category</button>
            </form>
        </div>
    </div>
    @endsection
    </div>
    </body>
    </html>
