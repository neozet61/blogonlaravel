@extends('layouts/admin_base')

    @section('content')
    <!--    content-->
    <div class="row">
        <div class="col-md-12">

            <div class="create d-flex flex-row-reverse">
                <div class="p-2">
                    <a href="/admin/create_category" class="btn btn-success">+ Create Category</a>
                </div>
            </div>

            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Actions</th>
                </tr>
                @foreach($allCategories as $category)
                <tr>
                    <td>
                        {{$category->id}}
                    </td>
                    <td>
                        {{$category->title}}
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <a href="/admin/edit_category?category_id={{$category->id}}" class="btn btn-warning">Edit</a>
                            <a href="/admin/delete_category?category_id={{$category->id}}" class="btn btn-danger">Delete</a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    @endsection
</div>
</body>
</html>
