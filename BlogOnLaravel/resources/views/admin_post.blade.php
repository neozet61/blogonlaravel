@extends('layouts/admin_base')
@section('content')
    <!--    content-->
    <div class="row">
        <div class="col-md-12">

            <div class="create d-flex flex-row-reverse">
                <div class="p-2">
                    <a href="/admin/create_post" class="btn btn-success">+ Create Post</a>
                </div>
            </div>

            <table class="table table-striped">
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Category</th>
                    <th>Actions</th>
                </tr>

                @foreach($allPosts as $post)
                <tr>
                    <td>
                        {{$post->id}}
                    </td>
                    <td>
                        {{$post->title}}
                    </td>
                    <td>
                        {{$post->category_title}}
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                            <a type="button" href="/admin/edit_post?post_id={{$post->id}}" class="btn btn-warning">Edit</a>
                            <a type="button" href="/admin/delete_post?post_id={{$post->id}}" class="btn btn-danger">Delete</a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
    @endsection
</div>
</body>
</html>
