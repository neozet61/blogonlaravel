@extends('layouts.admin_form_base')
@section('content')
    <!--    content-->
    <div class="row">
        <div class="col-md-12">
            <h2 id="add-new-post-title">{{ !isset($post) ? "Create" : "Edit" }} post: {{ $post->title ?? '' }}</h2>

            <form id="new-post-form" method="post" action="/admin/save_post">
                @csrf
                <div class="mb-3">
                    <label for="new-post-title" class="form-label">Title</label>
                    <input value="{{ $post->title ?? '' }}" type="text" name="new-post-title" class="form-control"
                           id="new-post-title">
                    @if(isset($post))
                        <input type="hidden" name="id" value="{{ $post->id }}">
                    @endif
                </div>
                <div class="mb-3">
                    <label for="new-post-category" class="form-label">Category</label>
                    <select class="form-control" name="new-post-category" id="new-post-category">
                        @foreach($allCategories as $category)
                            <option value="{{ $category->id }}"
                                {{ (isset($post) and $post->category_id == $category->id) ? "selected" : '' }}>
                                {{ $category->title }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label for="comment-input-text" class="form-label">Body</label>
                    <textarea placeholder="Write you post here..." name="comment-input-text" class="form-control"
                              id="comment-input-text">{{ $post->body ?? '' }}</textarea>
                </div>
                <button type="submit" class="btn btn-primary">Save post</button>
            </form>
        </div>
    </div>
    @endsection
    </div>
    </body>
    </html>
