@extends('layouts/base')
@section('content')
    <!-- post-->
    <div class="post">
        <h2 class="post-title">
            {{$post->title}}
        </h2>
        <div class="post-meta text-muted">
            <ul class="list-inline">
                <li class="datetime list-inline-item">
                    Date: {{\Carbon\Carbon::parse($post->time_code)->format('d.m.Y H:m:s')}}
                </li>
                <li class="list-inline-item">
                    Category: <a href="/?category_id={{$post->category_id}}">{{$post->category_title}}</a>
                </li>
            </ul>
        </div>
        <div class="post-body">
            <p>
                {{$post->body}}
            </p>
        </div>
    </div>

    @section('comments')
        <div id="comments">
            <h3 id="comments-title">Comments</h3>

            <!-- comment-->
            @foreach($comments as $comment)
                @if (!is_null($lastvizit) and ($lastvizit < $comment->time_at))
                    <div class="comments-item new-comment" id="comment_{{$comment->id}}">
                @else
                    <div class="comments-item" id="comment_{{$comment->id}}">
                @endif
                    <h5 class="comment-title">{{$comment->guest_name}}
                        <small class="text-muted">
                            at {{date('d.m.Y H:m:i', $comment->time_at)}}
                        </small>
                    </h5>
                    <div class="comment-body">
                        {{$comment->comment_text}}
                    </div>
                </div>
            @endforeach

        </div>



        <form id="new-comment-form" method="post" action="/save_comment">
            @csrf
            <div class="mb-3">
                <label for="comment-input-name" class="form-label">Name</label>
                <input placeholder="Your name" type="text" class="form-control" name="comment-input-name"
                       id="comment-input-name">
                <input type="hidden" name="post_id" value="{{$post->id}}">
            </div>
            <div class="mb-3">
                <label for="comment-input-text" class="form-label">Comment</label>
                <textarea placeholder="Write you message" class="form-control" name="comment-input-text"
                          id="comment-input-text"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Add comment</button>
        </form>

    @show
@endsection

